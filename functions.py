# functions make the code more readable and
# help to avcid copying and pasting code frequently
# Functions in python makes use of parameter
# Its nice to always add comments to make your code readable
# Functions most be declared before the line of code its Called

from datetime import datetime

# print timestamp to see how long sections of
# code take to run

# Without function call
first_name = 'Susan'
print('Task completed')
print(datetime.now())
print()

for x in range(0, 10):
    print(x)
print('Task Completed')
print(datetime.datetime.now())
print()

# Function to print current date and time
def print_time():
    print('Task completed')
    print(datetime.datetime.now())
    print()

    print_time()
    for x in range(0, 10):
        print(x)
print_time()