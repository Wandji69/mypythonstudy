# collins = {}
# collins['first'] = 'Wandji'
# collins['last'] = 'Collins'

# susan = {'first': 'Susan', 'last': 'Ibach'}

# print()
# # print(Collins)
# # print(susan)

# people = [collins, susan]
# people.append({'first': 'Bill', 'last': 'Gates'})
# presenters = people[0:2]
# # print(people)
# # print()
# # print(presenters)
# print(len(presenters))
# print()

### using the for loop and the while loop
people = ['Christopher', 'Susan']
print()
# for name in people:
#     print(name)
index = 0
while index < len(people):
    print(people[index])
    index = index + 1
print()