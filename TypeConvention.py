# first_number = input('Enter first number: ')
# second_num = input('Enter second: ')
# # print(first_number + second_num)

# print(int(first_number) + int(second_num))
# print(float(first_number) + int(second_num))

# pi = 3.14
# print(pi)

# first = 5
# second = 8
# print(first ** second)

## Dates in python

# To get current date and time
# We need the current datetime library
from datetime import datetime, timedelta

today = datetime.now()
print('Today is: ' + str(today))

# timedelta is used to define a period of time
one_day = timedelta(days=1)
yesterday = today - one_day

print('Yesterday was: ' + str(yesterday))

birthday = input('When is your birthday (dd/mm/yyy)? ')

birthday_date = datetime.strptime(birthday, '%d/%m/%Y')

print('Birthday: ' + str(birthday_date))

x = 42
y = 0

try:
    print(x / y)
except ZeroDivisionError as e:
    print("Sorry yours not allowed to divid by 0 ")
finally:
    print('This is are clean up code!')

    country = input('Enter the name off your country')
    if country.lower() == 'cameroon':
        print('So you most corruption')
    else:
        print('You are not from canada')