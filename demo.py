# Importing modules/packages
# Importing and entire package
import helpers
helpers.display('Sample message', True)

# importing a specific part of a module or a package
from helpers import display
display('Sample message')