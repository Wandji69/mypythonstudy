# Ask for someone names and return the name

# function to return the initials of a person

# def get_initials(name):
#     initail = name[0:1]
#     return initail

# first_name = input('Enter your first name: ')
# middle_name = input('Enter your middle name: ')
# last_name = input('Enter your last name: ')

# print('Your initials are: ' + get_initials(first_name) \
#     + get_initials(middle_name) \
#     + get_initials(last_name))

# Using functions with more than one parameter
# The function will take a name and return the 
# first letter of the name
def get_initials(name, force_uppercase):
    if force_uppercase:
        initial = name[0:1].upper()
    else:
        initial = name[0:1]
    return initial

# Ask for someone's name and return the initials 
first_name = input('Enter your first Name: ')
first_name_initial = get_initials(first_name, False)

print('Your initial is: ' + first_name_initial)