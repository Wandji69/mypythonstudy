province = input("What province do you live in? ")
tax = 0

if province == 'Alberta':
    tax = 0.05
if province == 'Nunavut':
    tax = 0.05
if province == 'Ontorai':
    tax == 0.13
print(tax)

# Using the ELIF statement
province = input("What province do you live in? ")
tax = 0

if province == 'Alberta':
    tax = 0.05
elif province == 'Nunavut':
    tax = 0.05
elif province == 'Ontorai':
    tax == 0.13
else:
    tax = 0.15
print(tax)

# Using the OR key word
province = input("What province do you live in? ")
tax = 0

if province == 'Alberta' or province == 'Nunavut':
    tax = 0.05
elif province == 'Ontorai':
    tax == 0.13
else:
    tax = 0.15
print(tax)

# Using the IN Key word
province = input("What province do you live in? ")
tax = 0

if province in ('Albarta', 'Nunavut', 'Yukon'):
    tax = 0.05
elif province == 'Ontorai':
    tax == 0.13
else:
    tax = 0.15
print(tax)

# Using Nested_if statement
country = input("What province do you live in? ")

if country.lower() == 'canada':
    province = input('What Province do you live in? ')
    if province in ('Alberta', \
        'Nunavut', 'Yukon'):
        tax = 0.05
    elif province == 'Ontario':
            tax = 0.13
    else:
            tax = 0.15
else:
    tax = 0.0
print(tax)

# student makes honour roll if their average is >=85
# and their lowes grade is not below 75

gpa = float(input('What was your Grade Point Average? '))
lowest_grade = float(input('What was your lowest grade? '))

# Codition to check grade
if gpa >= .85:
    if lowest_grade >= .70:
        print('Congratulations! You made the honour roll')

# Using AND keyword and Boolean (true/false)
if gpa >= .85 and lowest_grade >= .70:
    honour_roll = True
else:
    honour_roll = False

# Somewhere later in your code if you need to check the if statement is 
# on honour roll, all i need to do is check the boolean variable
# I set ealier in my code
if honour_roll:
    print('Congratulations! You made the honour roll')